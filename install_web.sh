#! /bin/bash
# Simple installation script for web vm
# mathis ~ 14/11/2021
dnf config-manager --add-repo=https://download.docker.com/linux/centos/docker-ce.repo
dnf update
dnf install -y docker-ce docker-ce-cli containerd.io
systemctl enable docker
systemctl start docker
groupadd --force --system docker
usermod -a -G docker $USER
docker volume create pufferpanel-servers
docker volume create pufferpanel-config
docker create --name pufferpanel -p 8080:8080 -p 5657:5657 -v pufferpanel-config:/etc/pufferpanel -v pufferpanel-servers:/var/lib/pufferpanel --restart=on-failure pufferpanel/pufferpanel:latest
docker start pufferpanel
docker inspect pufferpanel --format='{{range .Config.Env}}{{println .}}{{end}}'
