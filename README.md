# TP3 Linux : Projet personnel 

Réalisation d'un gestionnaire d'un serveur du jeu Minecraft . 

## **Sommaire** 
- [TP3 Linux : Projet personnel](#tp3-linux--projet-personnel)
  - [**Sommaire**](#sommaire)
  - [**Ressources nécessaires**](#ressources-nécessaires)
  - [I.Présentation :](#iprésentation-)
  - [II.Setup](#iisetup)
    - [1.PufferPanel](#1pufferpanel)

## **Ressources nécessaires**

- Script automatique de déploiement qui va contenir en ressource : 
    
    - `apt install git` pour pouvoir importer les scripts depuis notre git. 
    - `apt install default-jre` pour pouvoir exécuter le serveur minecraft (qui est en Java) lorsqu'il sera déployé. 
    - `bash <(curl -Ss https://my-netdata.io/kickstart.sh)` qui installera Netdata, un outil de monitoring pour pouvoir superviser l’activité des serveurs (RAM, CPU...).
    - `apt install curl` éventuellement si il n'est pas disponible, `curl` va nous servir à éxecuter le script d'installation de Netdata ci-dessus.
    - `apt install python3` éventuellement si il n'est pas déjà installé sur les machines par défaut. 

## I.Présentation :
Ce tp est un projet réalisé par 3 personnes:
Coco Mathis, Peyrataud Enzo et Fallous Lucas.
Nous avons voulu reprendre (et améliorer) le projet que nous avions fait en première année : [ici](https://gitlab.com/Lucazio/serveurs_minecraft_projet_infra)

Pour cela nous allons tenter de réaliser cette infrastructure:
![infrastructure](img/infrastructure.drawio.png)
dans notre infrastructure ces machines auront pour rôle:

`web.tp3`: Serveur web où l'on interagira avec le gestionnaire de serveurs de jeu

`proxy.tp3`: Reverse proxy qui sert d'intermédiaire entre le client et le serveur `web.tp3`

`jeu.tp3`: machine qui hébergera les serveurs de jeu

`backup.tp3`: Serveur qui aura pour rôle de récupérer les données provenant des autres serveurs et de les stocker de manière sûre

## II.Setup

### 1.PufferPanel
on va installer PufferPanel en utilisant Docker, pour le moment cela nous servira de serveur web mais nous espérons pouvoir développer le nôtre à la fin de ce projet pour pouvoir utiliser notre solution de B1.
```
[lucas@web ~]$ sudo dnf update
[lucas@web ~]$ sudo dnf install docker -y
[lucas@web ~]$ docker volume create pufferpanel-servers
[lucas@web ~]$ docker volume create pufferpanel-config
[lucas@web ~]$ docker create --name pufferpanel -p 8080:8080 -p 5657:5657 -v pufferpanel-config:/etc/pufferpanel -v pufferpanel-servers:/var/lib/pufferpanel --restart=on-failure pufferpanel/pufferpanel:java
[lucas@web ~]$ docker start pufferpanel
```
